var qs = function(route, obj) {
  route += '?';
  for(var item in obj) {
    var pair = item + '=' + obj[item] + '&';
    route += pair;
  } 

  return route.substring(0, route.length - 1);
};

window.app = angular.module('app', ['ngRoute']);

app.controller('index',  ['$scope','$http', app.index]);

app.controller('level0', ['$scope','$http', function($scope, $http) {

  $scope.actions = ['add', 'multiply', 'hello'];
  $scope.result = 'none';
  var getUrl = function(action, value1, value2) {
    var uri = 'http://localhost:8000/?action=' + action;
    uri += '&value1=' + value1;
    uri += '&value2=' + value2;

    return uri;
  }

  $scope.formSubmit = function() { 
    $http.get(getUrl($scope.action, $scope.value1, $scope.value2)).then(function(data) {
      $scope.result = data.data;
    });
  };

}]);

app.controller('level1', ['$scope','$http', '$location', function($scope, $http, $location) {
  $scope.users = [];

  $scope.getUser = function(user) {
    $http.get(qs('http://localhost:8000/users/get', {id : user.id})).then(function(data) {
      $scope.result = JSON.stringify(data, null, 2);
    });
  };

  $http.get('http://localhost:8000/users/list').then(function(data) {
    $scope.result = JSON.stringify(data, null, 2);
    $scope.users = data.data;
  });

  $scope.deleteUser = function(user) {
    $http.get(qs('http://localhost:8000/users/delete', {id : user.id})).then(function(data) { 
      $scope.result = JSON.stringify(data, null, 2);
    });
  };

  $scope.filter = function() {
    $http.get(qs('http://localhost:8000/users/find', {name : $scope.findBy})).then(function(data) { 
      $scope.users = data.data;
      $scope.result = JSON.stringify(data, null, 2);
    });
  };

  $scope.create = function() {
   var user = { name : $scope.name, age : $scope.age };
    $http.get(qs('http://localhost:8000/users/create', user)).then(function(data) {
      $scope.result = JSON.stringify(data, null, 2);
    });
  };
}]);

app.controller('level2', ['$scope','$http', '$location', function($scope, $http, $location) {

  $scope.users = [];

  $scope.getUser = function(user) {
    $http.get(('http://localhost:8000/user', {id : user.id})).then(function(data) {
      $scope.result = JSON.stringify(data, null, 2);
    });
  };

  $http.get('http://localhost:8000/users').then(function(data) {
    $scope.users = data.data;
    $scope.result = JSON.stringify(data, null, 2);
  });

  $scope.deleteUser = function(user) {
    $http.post('http://localhost:8000/user/delete', {id : user.id}).then(function(data) { 
      $scope.result = JSON.stringify(data, null, 2);
    });
  };

  $scope.filter = function() {
    $http.post('http://localhost:8000/users', {name : $scope.findBy}).then(function(data) { 
      $scope.users = data.data;
      $scope.result = JSON.stringify(data, null, 2);
    });
  };

  $scope.create = function() {
    $http.post('http://localhost:8000/user', { name : $scope.name, age : $scope.age }).success(function(data) {
      $scope.result = JSON.stringify(data, null, 2);
      $scope.users.push(data.data);
    });
  }
}]);

app.controller('level3', ['$scope','$http', '$location', function($scope, $http, $location) {
  var links;

  $http.get('http://localhost:8000/').then(function(data) {
    links = data.data._links;  

    $http.get(links.list.href).then(function(userlist) { 
      $scope.result = JSON.stringify(userlist, null, 2);
      $scope.users = userlist.data.data;
    });
  });

  $scope.create = function() {
    $http.put(links.create.href,
       JSON.stringify({ name : $scope.name, age : $scope.age })
    ).then(function(data) {
       $scope.result = JSON.stringify(data, null, 2);
    });
  };

  $scope.users = [];
  
  $scope.getUser = function(user) {
    $http.get(user._links.self.href).then(function(data) {
      $scope.result = JSON.stringify(data, null, 2);
    });
  };

  $scope.deleteUser = function(user) {
    $http.delete(user._links.remove.href).then(function(data) { 
      $scope.result = JSON.stringify(data, null, 2);
    });
  };

  $scope.filter = function() {
    var query = {name : $scope.findBy};
    console.log(query);
    $http.post(links.find.href, query).then(function(data) { 
      $scope.result = JSON.stringify(data, null, 2);
      $scope.users = data.data;
    });
  };
}]);

app.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/level0', {
    templateUrl: 'templates/level0.html'
  }).when('/level1', {
    templateUrl: 'templates/level1.html'
  }).when('/level2', {
    templateUrl: 'templates/level2.html'
  }).when('/level3', {
    templateUrl: 'templates/level3.html'
  }).when('/', {
    templateUrl: 'templates/index.html'
  })
}]);
