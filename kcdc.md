# [fit] Hypermedia 

---
# [fit] Hypertext
---

# [fit] HATEOAS

---

# Hypermedia as the Engine of Application State

---

![fit](sponsors.png)

--- 

# Agenda 

1. Me
1. REST
1. Levels of REST (Richardson Maturity Model)
1. Show me the code
1. Q&A

---

# Me

## __*@msarchet*__
### *msarchet.com*
### Microsoft
### Seattle


---

# REST

##  Use __*VERBS*__ and __*URI's*__ together

---

# Level __*0*__

## __*1*__ Endpoint __*No*__ Verbs

## __*Not*__ REST

___

# Level _*1*_

## __*1*__ Endpoint __*1*__ VERB

---

# Level _*2*_

## __*N*__ Endpoints __*2+*__ VERBS

___

# Level _*3*_

## __*N*__ Endpoints __*2+*__ VERBS

## Discoverability

---

# Code and 'Slides'

git.msarchet.com/kcdc-hypermedia
