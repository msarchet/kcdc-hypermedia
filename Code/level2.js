var url  = require('url'),
    util = require('util'),
    uuid = require('uuid'),
    method = require('./modules/methods'),
    ds   = require('./modules/database'),
    rest = method.rest;

ds.init();
userStore = ds.createStore('users');

var users = [];

userStore.get(null, function(err, data) { 
 users = data || users; 
});

var router = function(req, res) {
  var parsed = url.parse(req.url, true).pathname;

  switch(parsed) {
    case('/user'):
      console.log(req.method);
      if(req.method === 'GET') {
        rest.get(req, res, user.get);
      } else if(req.method === 'POST') {
        rest.post(req, res, user.create);
      }
      break;
    case('/users'):
      if(req.method === 'GET') {
        rest.get(req, res, user.list);
      } else {
        rest.post(req, res, user.find);
      }
      break;
    case('/user/delete'):
      rest.post(req, res, user.remove);
      break;
    default:
      res.writeHead(404, {'content-type' : 'text/plain'});
      res.end(util.format('No Resource at %s', parsed));
  }
};

var users = [];

var user = {
  create : function(req, res) {
    method.getRequestBody(req, res, function(req, res, body)  {
      var newUser = body;
      newUser.id = uuid.v4();
      users.push(newUser);
      userStore.save(newUser.id, newUser);
      res.end(JSON.stringify(newUser));
    });
  },
  list : function(req, res) {
    res.end(JSON.stringify(users));
  },
  find : function(req, res) {
    method.getRequestBody(req, res, function(req, res, body) {
      var found = users.map(function(u) {
        if(u.name.indexOf(body.name) !== -1) {
          return u;
        }
      });

      if(found) {
        res.end(JSON.stringify(found));
      } else {
        res.end(JSON.stringify([]));
      }
    });
  },
  remove : function(req, res) {
    method.getRequestBody(req, res, function(req, res, body) { 
      var removedUser;
      for(var u in users) {
        if(users[u].id === body.id) {
          removedUser = users[u];
          userStore.remove(removedUser.id);
          users.splice(u, 1);
        }
      }

      if(removedUser) {
        res.end(JSON.stringify(removedUser));
      } else {
        res.writeHead(404, { 'content-type': 'text/plain'});
        res.end('Not Found');
      }
    });
  },
  get : function(req, res) {
    var id = url.parse(req.url, true).query;
    var user;
    for(var u in users) {
      if(users[u].id === id.id) {
        user = users[u]
      }
    }

    if(user) {
      res.end(JSON.stringify(user));
    } else {
      res.writeHead(404, { 'content-type': 'text/plain'});
      res.end('Not Found');
    }
  }
};

var server = method.server(router);
