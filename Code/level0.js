var http = require('http'),
    url  = require('url'),
    util = require('util'),
    serverStart = require('./modules/methods');

var actioner = function(queryString, res) { 
  if(queryString && queryString.action) {

     switch(queryString.action) {
      case("add"):
        var val1 = parseInt(queryString.value1 || "0");
        var val2 = parseInt(queryString.value2 || "0");
        res.write((val1 + val2).toString());
        break;
      case("multiply"):
        var val1 = parseInt(queryString.value1 || "0");
        var val2 = parseInt(queryString.value2 || "0");
        res.write((val1 * val2).toString());
        break;
      case("hello"):
        res.write('Hello ' + queryString.value1);
        break;
      default:
        res.write('Unkown Action');
        break;
    }
  }
};

var server = serverStart.server(function(req, res) {
  var parsed = url.parse(req.url, true);
  var queryString = parsed.query;
  actioner(queryString, res);
  res.end();
});
