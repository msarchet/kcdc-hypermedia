var url  = require('url'),
    util = require('util'),
    uuid = require('uuid'),
    qs   = require('querystring'),
    method = require('./modules/methods'),
    ds   = require('./modules/database'),
    rest = method.rest;

ds.init();
userStore = ds.createStore('users');

var users = [];

userStore.get(null, function(err, data) { 
 users = data || users; 
});

var router = function(req, res) {
  var parsed = url.parse(req.url, true).pathname;
  switch(parsed) {
    case('/user'):
      if(req.method === 'GET') { 
        rest.get(req, res, user.get);
      } else if (req.method === 'PUT') {
        rest.put(req, res, user.create);
      } if (req.method === 'DELETE') {
        rest.del(req, res, user.remove);
      }
      break;
    case('/users'):
      if(req.method === 'GET') {
        rest.get(req, res, user.list);
      } else {
        rest.post(req, res, user.find);
      }
      break;
    case('/'): 
      rest.get(req, res, function(req, res) {
        var links =  { _links : 
          {
            create  : { href : 'http://localhost:8000/user'  },
            get     : { href : 'http://localhost:8000/user'  }, 
            list    : { href : 'http://localhost:8000/users' },
            find    : { href : 'http://localhost:8000/users' }
          }
        };
        res.writeHead(200, {'content-type' : 'application/json'});
        res.write(JSON.stringify(links));
        res.end();
      });
      break;
    default:
      res.writeHead(404, {'content-type' : 'text/plain'});
      res.end();
  }
};

var users = [];

var user = {
  addLinks : function(u) {
    return { 
      data : u, 
      _links : {
        self :   { href : 'http://localhost:8000/user?id=' + u.id },
        remove : { href : 'http://localhost:8000/user?id=' + u.id}
      }
    }
  },
  create : function(req, res) {
    var userData = "";

    req.on('data', function(chunk) {
        userData += chunk;
    });

    req.on('end', function()  {
      var newUser = JSON.parse(userData);
      newUser.id = uuid.v4();
      users.push(newUser);
      userStore.save(newUser.id, newUser);
      res.end(JSON.stringify(user.addLinks(newUser)));
    });
  },
  list : function(req, res) {
    var result = {
      _links : {
        self : { href : 'http://localhost:8000/users' },
      },
      data : users.map(function(u) { return user.addLinks(u); })
    };
    res.end(JSON.stringify(result));
  },
  find : function(req, res) {
    var params = url.parse(req.url, true).query;
    var found = users.filter(function(u) {
      if(u.name.indexOf(params.name) !== -1) {
        return u;
      }
    });
    found = found || []; 
    var result =  {
      _links :  { 
        self : { href : 'http://localhost:8000/users?' + qs.encode(params) },
      } ,
      data : found.map(function(u) { return user.addLinks(u); })
    }
    res.end(JSON.stringify(result));
  },
  remove : function(req, res) {
    var params = url.parse(req.url, true).query;
    var removedUser;
    for(var u in users) {
      if(users[u].id === params.id) {
        removedUser = users[u];
        userStore.remove(removedUser);
        users.splice(u, 1);
      }
    }

    if(removedUser) {
      res.end(JSON.stringify(removedUser));
    } else {
      res.writeHead(404, { 'content-type': 'text/plain'});
      res.end('Not Found');
    }
  },
  get : function(req, res) {
    var id = url.parse(req.url, true).query;
    var foundUser;
    for(var u in users) {
      if(users[u].id === id.id) {
        foundUser = users[u]
      }
    }
    if(user) {
      res.end(JSON.stringify(user.addLinks(foundUser)));
    } else {
      res.writeHead(404, { 'content-type': 'text/plain'});
      res.end('Not Found');
    }
  }
};

var server = method.server(router);
