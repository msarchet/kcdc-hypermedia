var fs = require('fs'),
    util = require('util');

var saveData = function(filename, data) {
  var jsonRepresentation = JSON.stringify(data);

  fs.writeFile(filename, jsonRepresentation, function(err) {
    if(err) {
      util.log(util.format('ERROR: Unable to save data to %s', filename));
      return;
    }

    util.log(util.format('LOG: Wrote data to %s', filename)); 
  });
};


var readData = function(filename, callback) {
  fs.readFile(filename, function(err, data) {
    if(err) {
      util.log(util.format('ERROR: Unable to read data from %s', filename));
      util.log(err);
      callback(null, null);
      return;
    } 
    util.log(util.format('LOG : Reading data from %s : %s', filename, JSON.stringify(data)));
    callback(null, JSON.parse(data));
  });
};

var getPath = function(path, id) {
  return getReadPath(path, id) + '.json';
};

var getReadPath = function(path, id) {
  return path + '/' + id; 
};

var dataStore = function() {
  var initialized = false;

  var init = function() {
    fs.mkdir('data', function(err) {
      if(err) {
          util.log(err);  
      }
      
      initialized = true;
    });
  };

  var createStore = function(objectName) {
    if(!initialized) {
      init();  
    }

    var path = './data/' + objectName;
    fs.mkdir(path, function() { });

    var save = function(id, data) {
      saveData(getPath(path, id), data);   
    };  

    var get = function(id, callback) {
      
      if(id) {
        readData(getPath(path, id), callback);
      } else {
        getDir(callback);
      }
    };

    var getDir = function(callback) { 
        // get a list of all files in the object directory
        fs.readdir(path, function(err, files) {
          
          if(err) {
            util.log(util.format('ERROR: Unable to read files from %s', path));
            callback(err);
            return;
          }

          var objects = [];

          // start reading each file
          files.forEach(function(f) {
          
            readData(getReadPath(path,f), function(_err, data) { 
              // add object to return item
              objects.push(data);
              
              // if we've read as many objects as there are files return
              // need to handle file reads that error
              if(objects.length === files.length) {
                callback(null, objects); 
              }
            });
          });
        });
    };

    // remove and re-add the object directory
    var clear = function(callback) {
      fs.rmdir(path, function(err) { 
        if(err) {
          callback(err);
          return; 
        }
        fs.mkdir(path); 
      });
    };

    var remove = function(id) {
      fs.unlink(getPath(path, id),function(err) {
        if(err) {
          util.log(err);
        }
      });
    };

    return {
      save : save,
      get  : get,
      clear : clear,
      remove : remove
    }
  };
  
  return {
    init : init,
    createStore : createStore
  }  
};

module.exports = dataStore();
