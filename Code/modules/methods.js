var util = require('util'),
    http = require('http');

var get = function(req, res, callback) {
  generic('GET', req, res, callback);
};

var post = function(req, res, callback) {
  generic('POST', req, res, callback);
};

var put = function(req, res, callback) {
  generic('PUT', req, res, callback);
};

var patch = function(req, res, callback) {
  generic('PATCH', req, res, callback);
};

var del = function(req, res, callback) {
  generic('DELETE', req, res, callback);
};

var generic = function(method, req, res, callback) {
  if(req.method !== method) {
    res.setHeader(405, { 'content-type' : 'plain/text'}); 
    res.end('Method Not Allowed');
  } else {
    callback(req, res); 
  }
};

var writeError = function(statusCode, message, req, res) {
  res.writeHead(statusCode, { 'content-type' : 'text/plain'});
  res.end(message);
};

var serverStart = function(serverMethod, port) {
  var server = http.createServer(function (req, res) {
    util.log(util.format('server recieved request @ %s : %s', req.url, req.method));
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.setHeader('content-type', 'application/json');
    

    if(req.method === 'OPTIONS') {
      res.setHeader('Access-Control-Allow-Origin', '*');
      res.setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
      res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
      res.end();
    } else {
      serverMethod(req, res);
    }
  });

  server.listen(port || 8000);

  util.log(util.format('Server Running on port %s', port || 8000));
};

var getRequestBody = function(req, res, callback) {
  var data;

  req.on('data', function(chunk) { 
    if(!data) {
      data = chunk;
    } else {
      data += chunk;
    }
  });

  req.on('end', function() {
    console.log(data);
    callback(req, res, JSON.parse(data));
  });
};

exports.rest = {
  get : get,
  post : post,
  put : put,
  patch : patch,
  del: del
};
exports.getRequestBody = getRequestBody;
exports.server = serverStart;
exports.writeError = writeError;
