var url  = require('url'),
    util = require('util'),
    uuid = require('uuid'),
    method = require('./modules/methods'),
    ds   = require('./modules/database'),
    rest = method.rest;

ds.init();
userStore = ds.createStore('users');

var users = [];

userStore.get(null, function(err, data) { 
 users = data || users; 
});


var router = function(req, res) {
  var parsed = url.parse(req.url, true).pathname;
  switch(parsed) {
    case('/users/create'):
      rest.get(req, res, user.create);
      break;
    case('/users/list'):
      rest.get(req, res, user.list);
      break;
    case('/users/find'):
      rest.get(req, res, user.find);
      break;
    case('/users/get'):
      rest.get(req, res, user.get);
      break;
    case('/users/delete'):
      rest.get(req, res, user.remove);
      break;
  } 
};

var user = {
  create : function(req, res) {
    var user = url.parse(req.url, true).query;
    user.id = uuid.v4();
    util.log(util.format('LOG : Saving user %s', JSON.stringify(user)));
    userStore.save(user.id, user);

    users.push(user);
    res.end(JSON.stringify(user));
  },
  list : function(req, res) {
    res.end(JSON.stringify(users));
  },
  find : function(req, res) {
    var data = url.parse(req.url, true).query; 
    var name = data.name;
    var found = users.filter(function(u) { 
      if(u.name.indexOf(name) !== -1) { 
        return u; 
      }
    });

    if(found) { 
      res.end(JSON.stringify(found));
    } else {
      res.end([]);
    }
  },
  remove : function(req, res) {
    var data = url.parse(req.url, true).query; 
    var removedUser;
    for(var u in users) {
      if(users[u].id === data.id) {
        removedUser = users[u];
        userStore.remove(removedUser.id);
        users.splice(u, 1); 
      }  
    }

    if(removedUser) {
      res.end(JSON.stringify(removedUser)); 
    } else {
      res.writeHead(404, { 'content-type' : 'text/plain' });
      res.end('Not Found');  
    }
  },
  get : function(req, res) {
    var data = url.parse(req.url, true).query; 
    var user;
    for(var u in users) {
      if(users[u].id === data.id) {
        user = users[u]
      } 
    }
    if(user) {
      res.end(JSON.stringify(user)); 
    } else {
      res.writeHead(404, { 'content-type' : 'text/plain' });
      res.end('Not Found');  
    }
  }
};

var server = method.server(router); 
